export default [
  {
    path: '*',
    redirect: '/404'
  },
  {
	path: '',
    component: () => import('@/views/Index'),
    children: [
      {
        path: '/image',
        component: () => import('@/views/ImageView'),
        meta: {
          title: 'AppHeader'
        }
      }
    ]
  }
]
